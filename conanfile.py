from conans import ConanFile, AutoToolsBuildEnvironment, tools


class NautyConan(ConanFile):
    name = "nauty"
    version = "2.7r1"
    license = "Apache-2.0"
    author = "Harald Held <harald.held@gmail.com>"
    url = "https://gitlab.com/coinor-conan/nauty-conan"
    description = "nauty and Traces are programs for computing automorphism groups of graphs and digraphs."
    topics = ("graph automorphism")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"

    def source(self):
        tools.get("http://pallini.di.uniroma1.it/nauty27r1.tar.gz")

    def build(self):
        autotools = AutoToolsBuildEnvironment(self)

        with tools.chdir("nauty27r1"):
            autotools.configure()
            autotools.make()

    def package(self):
        self.copy("nauty.h", dst="include", src="nauty27r1", keep_path=False)
        self.copy("nausparse.h", dst="include", src="nauty27r1", keep_path=False)
        self.copy("nauty.a", dst="lib", src="nauty27r1", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["nauty"]
